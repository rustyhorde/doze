// Copyright (c) 2018 doze developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! HTTP releated stuff
use error::Error;
use std::convert::TryFrom;
use std::fmt;
use uuid::Uuid;

/// HTTP Request Methods
///
/// ## Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content
/// [RFC7231](https://tools.ietf.org/html/rfc7231#section-4)
///
/// ## PATCH Method for HTTP
/// [RFC5789](https://tools.ietf.org/html/rfc5789#section-2)
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum RequestMethod {
    Get,
    Head,
    Post,
    Put,
    Delete,
    Connect,
    Options,
    Trace,
    Patch,
}

impl<'a> TryFrom<&'a str> for RequestMethod {
    type Error = Error;

    fn try_from(val: &str) -> ::std::result::Result<Self, Self::Error> {
        Ok(match val {
            "GET" => RequestMethod::Get,
            "HEAD" => RequestMethod::Head,
            "POST" => RequestMethod::Post,
            "PUT" => RequestMethod::Put,
            "DELETE" => RequestMethod::Delete,
            "CONNECT" => RequestMethod::Connect,
            "OPTIONS" => RequestMethod::Options,
            "TRACE" => RequestMethod::Trace,
            "PATCH" => RequestMethod::Patch,
            _ => return Err("Invalid HTTP Verb".into()),
        })
    }
}

impl fmt::Display for RequestMethod {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match *self {
                RequestMethod::Get => "GET",
                RequestMethod::Head => "HEAD",
                RequestMethod::Post => "POST",
                RequestMethod::Put => "PUT",
                RequestMethod::Delete => "DELETE",
                RequestMethod::Connect => "CONNECT",
                RequestMethod::Options => "OPTIONS",
                RequestMethod::Trace => "TRACE",
                RequestMethod::Patch => "PATCH",
            }
        )
    }
}

/// A Kroger API Request
#[derive(Clone, Debug, Eq, Getters, Hash, MutGetters, PartialEq, Setters)]
pub struct KRequest {
    /// The HTTP `RequestMethod`.
    #[get = "pub"]
    #[set = "pub"]
    #[get_mut = "pub"]
    method: RequestMethod,
    /// The correlation ID to be used for the X-Correlation-Id header.
    #[get = "pub"]
    #[set = "pub"]
    #[get_mut = "pub"]
    correlation_id: Uuid,
    /// The username that will be encoded into the Basic authentication header.
    #[get = "pub"]
    #[set = "pub"]
    #[get_mut = "pub"]
    username: String,
    /// The password that will be encoded into the Basic authentication header.
    #[get = "pub"]
    #[set = "pub"]
    #[get_mut = "pub"]
    password: String,
    /// The endpoint URL.
    #[get = "pub"]
    #[set = "pub"]
    #[get_mut = "pub"]
    url: String,
}

impl Default for KRequest {
    fn default() -> Self {
        Self {
            method: RequestMethod::Get,
            correlation_id: Uuid::nil(),
            username: Default::default(),
            password: Default::default(),
            url: Default::default(),
        }
    }
}

impl fmt::Display for KRequest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} -H \"X-Correlation-Id: {}\" -H \"Basic: ****\" {}",
            self.method,
            self.correlation_id.hyphenated(),
            self.url
        )
    }
}

#[cfg(test)]
mod tests {
    use super::{KRequest, RequestMethod};
    use std::convert::TryFrom;
    use uuid::Uuid;

    const FAIL: &str = "Failed to convert request method string";

    #[test]
    fn conversion() {
        assert_eq!(
            RequestMethod::Get,
            RequestMethod::try_from("GET").expect(FAIL)
        );
        assert_eq!(
            RequestMethod::Head,
            RequestMethod::try_from("HEAD").expect(FAIL)
        );
        assert_eq!(
            RequestMethod::Post,
            RequestMethod::try_from("POST").expect(FAIL)
        );
        assert_eq!(
            RequestMethod::Put,
            RequestMethod::try_from("PUT").expect(FAIL)
        );
        assert_eq!(
            RequestMethod::Delete,
            RequestMethod::try_from("DELETE").expect(FAIL)
        );
        assert_eq!(
            RequestMethod::Connect,
            RequestMethod::try_from("CONNECT").expect(FAIL)
        );
        assert_eq!(
            RequestMethod::Options,
            RequestMethod::try_from("OPTIONS").expect(FAIL)
        );
        assert_eq!(
            RequestMethod::Trace,
            RequestMethod::try_from("TRACE").expect(FAIL)
        );
        assert_eq!(
            RequestMethod::Patch,
            RequestMethod::try_from("PATCH").expect(FAIL)
        );

        match RequestMethod::try_from("BlAH") {
            Ok(_) => assert!(false, "BlAH is not a valid request method!"),
            Err(e) => assert_eq!(e.description(), "Invalid HTTP Verb"),
        }
    }

    #[test]
    /// svcPlatSOAT:nWuAvWm6rXfFkzNep1IMwI1E
    fn requests() {
        let default: KRequest = Default::default();
        assert_eq!(
            default.to_string(),
            "GET -H \"X-Correlation-Id: 00000000-0000-0000-0000-000000000000\" -H \"Basic: ****\" "
        );

        let mut mine: KRequest = Default::default();
        let correlation_id =
            Uuid::parse_str("2EE340FD-913D-4864-A795-F99CF6B728BD").expect("Can't parse UUID");
        mine.set_method(RequestMethod::Post);
        mine.set_correlation_id(correlation_id);
        mine.set_url("https://google.com".to_string());

        assert_eq!(mine.to_string(), "POST -H \"X-Correlation-Id: 2ee340fd-913d-4864-a795-f99cf6b728bd\" -H \"Basic: ****\" https://google.com");
    }
}
