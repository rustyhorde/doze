// Copyright (c) 2018 doze developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `RESTful` API Testing Framework
#![feature(try_from)]
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate getset;
#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;
extern crate uuid;

mod error;
mod http;

// Public API
pub use http::{KRequest, RequestMethod};

use error::Result;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Empty;

pub fn doze<'a, T, U>(_request: &KRequest, _body: &Option<T>) -> Result<U>
where
    T: Deserialize<'a>,
    U: Deserialize<'a>,
{
    Ok(serde_json::from_str("{\"test\": \"abc\"}")?)
}

#[cfg(test)]
mod test {
    use super::doze;

    #[derive(Deserialize)]
    pub struct Test {
        test: String,
    }

    #[test]
    fn dozer() {
        let testme = doze::<(), Test>(&Default::default(), &None).expect("FAIL");
    }
}
